// lastfm api stuff

    const lastfm_url = "https://ws.audioscrobbler.com/2.0/?method=user.getrecenttracks&user=lule345&api_key=92970b5c49482b273fb04ed1250b9144&format=json";
    const lastfm_url_top = "https://ws.audioscrobbler.com/2.0/?method=user.getlovedtracks&user=lule345&api_key=92970b5c49482b273fb04ed1250b9144&format=json";

    fetch(lastfm_url)
      .then(response => {
        if (!response.ok) {
          throw new Error(`HTTP error! Status: ${response.status}`);
        }
        return response.json(); // Changed from blob() to json()
      })
      .then(data => {
        let jukebox = document.querySelector("#jukebox_title");
      jukebox.textContent = "Recently Played: " + data.recenttracks.track[0].name + " by " + data.recenttracks.track[0].artist['#text'];
      })
      .catch(error => {
        console.error('Error fetching data:', error);
      });