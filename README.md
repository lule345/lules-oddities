<img src="https://badgen.net/static/lules-oddities/yipee/green/?icon=github">

# Lule's Oddities
A work in progress landing page to showoff my profile.
created by lule345

8/31/2023:
Half way done, Just need to fix some stuff, add some fonts and then finally put in my bio. I need wallpaper recommendations, Matrix gif is too broad of a wallpaper.

2/22/2024:
I'd say the website is complete, though the "blog" in there is pretty much dead as I'm too lazy to update it. Enjoy it if you want, and please contribute to this project by creating/solving issues and creating pull requests!

6/25/2024:
Almost a year huh? Can't believe it as well. That previous update above this one is what I call "Version 1", and guess what? Version 2 is essentially complete! I just want to create a [88x31](https://hekate2.github.io/buttonmaker/) button ad, and a [180x180](https://dimden.dev/navlinkads/) square ad, feel free to help! For updates in this version:

1. Photography/Art Page - View it [here!](https://lules-oddities.vercel.app/gallery.html)
2. Welcome page - Helps with warning mobile users and activating the new MIDI player!
3. Lots of widgets and stuff! - Some are interactable, how many can you find?
4. /log.html - [REDACTED]
5. Music Player - Previously, the music button was a janky, duct-taped button that played only one song; Now it supports both mp3 and MIDIs! This includes a new MIDI rendition of Atomic Amnesia by 3KilksPhillip
6. About Me Page - Now you know who I am! [link](https://lules-oddities.vercel.app/aboutme.html)
7. QoL Improvements! - Everything centered and (more or less) nicely designed